//
//  ViewController.swift
//  SliderProject
//
//  Created by My Mac on 11/08/21.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var scrlView: UIScrollView!
    @IBOutlet weak var pageControl: UIPageControl!
    
    var startButton = UIButton()
    var arrowButton = UIButton()
    let screen =  UIScreen.main.bounds
    var currentPage = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        designScrollView()
    }

    func designScrollView(){
    var count = 0
    var xPos = 0.0
    
    while count < 4 {
        
        let displayLabel = UILabel(frame: CGRect(x: xPos + 30, y: 50, width: Double(screen.width - 60), height: 100))
        displayLabel.numberOfLines = 0
        displayLabel.textColor = .white
        
        let imageView = UIImageView(frame: CGRect(x: xPos, y: 0.0, width: Double(screen.width), height: Double(screen.height)))
        imageView.contentMode = .scaleAspectFit
        
        if (count == 0 ) {
            imageView.image = UIImage(named: "wscreen1")
            displayLabel.text = "Does Your Vehicle Distrupt\nYour Smooth Jouney, "
            displayLabel.textAlignment = .right
        }else if (count == 1) {
            imageView.image = UIImage(named: "wscreen2")
            displayLabel.text = "We Are Here For\nThe Solution, "
            displayLabel.textAlignment = .center
        }else if (count == 2){
            imageView.image = UIImage(named: "wscreen3")
            displayLabel.text = "Toe you and your vehicle\nout of trouble, "
            displayLabel.textAlignment = .right
        
        }else {
            imageView.image = UIImage(named: "wscreen4")
            displayLabel.text = "Register Soon, "
            displayLabel.textAlignment = .right
            imageView.contentMode = .scaleToFill
        }
        scrlView.addSubview(displayLabel)
        scrlView.addSubview(imageView)
        xPos = xPos + Double(screen.width)
        count = count + 1
    }
        currentPage = 0
        pageControl.numberOfPages = 4
        scrlView.contentSize = CGSize(width: xPos, height: Double(screen.height))
        
        arrowButton = UIButton(type: .system)
        arrowButton.frame = CGRect(x: screen.width - 90, y: self.view.frame.height - 150, width: 50, height: 50)
        arrowButton.layer.cornerRadius = 0.5 * arrowButton.bounds.size.width
        arrowButton.backgroundColor = .white
        arrowButton.addTarget(self, action: #selector(arrowButtonClicked(sender:)), for: .touchUpInside)
        let image2 = UIImage(named: "right_arrow_purple");
        arrowButton.setImage(image2, for: .normal)
        self.view.addSubview(arrowButton)
        
        startButton = UIButton(type: .system)
        startButton.frame = CGRect(x: screen.width - 100, y: self.view.frame.height - 150, width: 90, height: 50)
        startButton.layer.cornerRadius = 25
        startButton.addTarget(self, action: #selector(startButtonAction(sender:)), for: .touchUpInside)
        startButton.setTitle("Start", for: .normal)
        let image4 = UIImage(named: "right_arrow_purple");
        startButton.setImage(image4, for: .normal)
        self.view.addSubview(startButton)
        startButton.isHidden = true
        
    }
    @objc func arrowButtonClicked(sender: UIButton) {
        currentPage = currentPage + 1
        scrlView.setContentOffset(CGPoint(x: currentPage * Int(screen.width), y: 0), animated: true)
        pageControl.currentPage = currentPage
        if currentPage == 3 {
            startButton.isHidden = false
            arrowButton.isHidden = true
        }
        else {
            startButton.isHidden = true
            arrowButton.isHidden = false
        }
    }
    
    @objc func startButtonAction(sender: UIButton) {
        
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let xPos = (scrollView.contentOffset.x)/screen.width
        pageControl.currentPage = Int(xPos)
        if Int(xPos) == 3 {
            startButton.isHidden = false
            arrowButton.isHidden = true
        }
        else {
            startButton.isHidden = true
            arrowButton.isHidden = false
        }
    }
}
